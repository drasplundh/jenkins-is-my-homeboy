package dev.asplundh.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


// a simple class to create a connection to our Database
public class ConnectionUtil {

    private static Connection connection;
    public static Connection getConnection() throws SQLException {

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        // check to see if connection is null or closed
        // use environment variables as credentials to connect to DB
        if (connection == null || connection.isClosed()) {
            String connectionUrl = "jdbc:postgresql://drasplundh-java-azure-training.postgres.database.azure.com:5432/postgres";
            String username = "drasplundh@drasplundh-java-azure-training";
            String password = "GumChunks52!";
            //String connectionUrl = System.getenv("connectionUrl");
            //String username = System.getenv("username");
            //String password = System.getenv("password");
            //System.out.println(connectionUrl);
            //System.out.println(username);
            //System.out.println(password);

            connection = DriverManager.getConnection(connectionUrl, username, password);
        }
        return connection;
    }
}
