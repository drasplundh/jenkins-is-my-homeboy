package dev.asplundh.data;

import dev.asplundh.entities.AuthResponse;
import dev.asplundh.models.UserModel;
import dev.asplundh.services.UserService;
import dev.asplundh.util.ConnectionUtil;
import io.javalin.http.UnauthorizedResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

public class UserLoginDaoImpl implements UserLoginDao {

    private Logger logger = LoggerFactory.getLogger(UserLoginDaoImpl.class);

    public UserModel getLoginCredentials(UserModel loginUser) {
        UserModel user = new UserModel();
        try (Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("select username, password from users where username = (?)");
            preparedStatement.setString(1, loginUser.getUsername());
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                String userName = resultSet.getString("username");
                String password = resultSet.getString("password");
                user.setUsername(userName);
                user.setPassword(password);
                //System.out.println("userName " + userName + " password " + password);
            }

        } catch (SQLException e) {
            logger.error(e.getClass() + " " + e.getMessage());
        }
        return user;
    }
}
