package dev.asplundh.data;

import dev.asplundh.models.CarModel;
import dev.asplundh.models.MakeModel;
import dev.asplundh.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class MakeModelHibDaoImpl implements MakeModelDao {

    @Override
    public List<MakeModel> getEvery() {
        try (Session s = HibernateUtil.getSession()) {
            List<MakeModel> makes = s.createQuery("from MakeModel", MakeModel.class).list();
            System.out.println(makes);
            return makes;

        }
    }

    public void deleteMake(String name) {
        System.out.println("name = " + name);
        try (Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
//            MakeModel deleteMake = new MakeModel(name);
            Query<MakeModel> query = s.createQuery("delete from MakeModel where name = :name");
            query.setParameter("name", name);
            query.executeUpdate();
            tx.commit();
        }

    }

    public void addMakeName(MakeModel make) {
        System.out.println("Make name is: " + make.getName());
        System.out.println("Region is: " + make.getRegion());
        try (Session s = HibernateUtil.getSession()) {
            s.beginTransaction();
            s.save(make);
            s.getTransaction().commit();
            s.close();
//            Transaction tx = s.beginTransaction();
//            Query<MakeModel> query = s.createQuery("insert into MakeModel");
//            query.executeUpdate();
//            tx.commit();
        }
    }
}
