package dev.asplundh.data;

import dev.asplundh.models.BigCarModel;
import dev.asplundh.models.CarModel;
import dev.asplundh.models.CarSpeed;

import java.util.List;

public interface CarModelDao {
    public List<CarModel> getAllModels();
    public CarModel getModelByName(String name);
//    public void addNewCarModel(String makeId, String makeName, String region, String modelName, String engine, String topSpeed,
//                               String zeroToSixty, String mpg, String price);
    public List<CarSpeed> raceByTopSpeed(String name1, String name2);
    public List<CarSpeed> raceByAcceleration(String name1, String name2);
    public void putCarModel(String oldName, String newName);
    public void deleteCarModel(String name);
    public boolean addNewCarModel(CarModel model);
}
