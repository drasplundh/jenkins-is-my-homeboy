package dev.asplundh.data;

import dev.asplundh.models.MakeModel;

import java.util.List;

public interface MakeModelDao {
    public List<MakeModel> getEvery();
}
