package dev.asplundh.data;


import dev.asplundh.models.CarModel;
import dev.asplundh.models.CarSpeed;
import dev.asplundh.util.HibernateUtil;
import io.javalin.http.NotFoundResponse;
import javassist.NotFoundException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.TransactionException;
import org.hibernate.query.Query;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.sql.SQLException;
import java.sql.SQLOutput;
import java.util.List;

public class CarModelHibDaoImpl implements CarModelDao {

    @Override
    public List<CarModel> getAllModels() {
        try (Session s = HibernateUtil.getSession()) {
            List<CarModel> models = s.createQuery("from CarModel", CarModel.class).list();
            System.out.println(models);
            return models;
        } catch (NoResultException error) {
            return null;
        }
    }

    public CarModel getModelByName(String name) {
        CarModel car = new CarModel();
        try (Session s = HibernateUtil.getSession()) {
            CriteriaBuilder cb = s.getCriteriaBuilder();
            CriteriaQuery<CarModel> cr = cb.createQuery(CarModel.class);
            Root<CarModel> root = cr.from(CarModel.class);
            cr.select(root).where(cb.like(root.get("name"), name));

            Query<CarModel> query = s.createQuery(cr);
            car = query.getSingleResult();

        } catch (NoResultException error) {
            return null;
        }
        return car;
    }

    // add a new car
    public boolean addNewCarModel(CarModel model) {
        try (Session s = HibernateUtil.getSession()) {
                s.beginTransaction();
                s.save(model);
                s.getTransaction().commit();
                s.close();
                return true;
        } catch (TransactionException error) {
            error.printStackTrace();
            return false;
        }
    }

    @Override
    public List<CarSpeed> raceByTopSpeed(String name1, String name2) {
        return null;
    }

    @Override
    public List<CarSpeed> raceByAcceleration(String name1, String name2) {
        return null;
    }

    @Override
    public void putCarModel(String oldName, String newName) {
        try (Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            Query<CarModel> query = s.createQuery("update CarModel set name = :newName where name = :oldName");
            query.setParameter("newName", newName);
            query.setParameter("oldName", oldName);
            query.executeUpdate();
            tx.commit();
        } catch (TransactionException error) {
            error.printStackTrace();
            //logger.warn("could not update the car model");
        }
    }


    @Override
    public void deleteCarModel(String name) {
        System.out.println("name = " + name);
        try (Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            Query<CarModel> query = s.createQuery("delete from CarModel where name = :name");
            query.setParameter("name", name);
            query.executeUpdate();
            tx.commit();
        }
    }
}
