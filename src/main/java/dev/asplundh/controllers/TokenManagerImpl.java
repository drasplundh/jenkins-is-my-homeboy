package dev.asplundh.controllers;

import io.javalin.http.ForbiddenResponse;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

import java.security.Key;

public class TokenManagerImpl implements TokenManager {

    private final Key key;

    // creates a secret key
    public TokenManagerImpl() {
        this.key = Keys.secretKeyFor(SignatureAlgorithm.HS256);
    }  // encryption function

    // creates a token for a given username
    @Override
    public String issueToken(String userId) {
        String token = Jwts.builder().setSubject(userId).signWith(key).compact();
        return token;
    }

    // checks if token is valid against a given username
    @Override
    public boolean authorize(String token, String userId) {
        try {
            // decryption function
            String subject = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token).getBody().getSubject();
            return subject.equalsIgnoreCase(userId);
        } catch (Exception e) {
            throw new ForbiddenResponse();
        }
    }
}
