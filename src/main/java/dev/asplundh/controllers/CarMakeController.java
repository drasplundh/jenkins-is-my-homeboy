package dev.asplundh.controllers;

import dev.asplundh.models.MakeModel;
import dev.asplundh.services.MakeModelService;
import io.javalin.http.Context;

import java.util.List;


public class CarMakeController {

    private MakeModelService service = new MakeModelService();

        public void handleGetCarMakeRequest(Context ctx) {
            ctx.json(service.getAllMakes());                             // call the service method for getAll()
            //logger.info("Getting all car makes");                  // Log if successful
            ctx.status(200);                                        // return normal status code to client
        }

        public void handleDeleteMakeByNameRequest(Context ctx) {
            String name = ctx.formParam("makeName");
            service.deleteMakeByName(name);
            ctx.status(200);
        }

        public void handleAddMakeRequest(Context ctx) {
            MakeModel make = ctx.bodyAsClass(MakeModel.class);
//            String name = ctx.formParam("makeName");
//            String region = ctx.formParam("region");
            service.addMake(make);
            ctx.status(200);
        }
    }
