package dev.asplundh.controllers;

import dev.asplundh.entities.AuthResponse;
import dev.asplundh.models.UserModel;
import dev.asplundh.services.UserService;
import io.javalin.http.Context;
import io.javalin.http.ForbiddenResponse;
import io.javalin.http.UnauthorizedResponse;
import jdk.nashorn.internal.parser.Token;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import java.security.Key;

public class AuthController {

    private TokenManager manager = new TokenManagerImpl();
    private Logger logger = LoggerFactory.getLogger(AuthController.class);
    private UserService loginUser = new UserService();


    // this method handles the login logic
    public void login(Context ctx) {
        String user = ctx.formParam("username");                        // take in username from client
        String pass = ctx.formParam("password");                        // take in password from client

        UserModel userLogin = new UserModel(user, pass);                    // create a UserModel object to be passed
                                                                            // to Database to grab credentials

        // this method gets the user's login credentials from the database
        // if the credentials match, we have a successful login, and pass
        // a JWT to the client.
        boolean loginSuccess = loginUser.getCredentials(userLogin);
        if (loginSuccess) {
            logger.info("User: " + user + " has successfully logged in");
            ctx.result(user);
            ctx.header("Authorization", manager.issueToken(user));

        }

    }
        // this method validates the token we passed earlier
        // for a given user.  We can use this method to validate
        // if a user can access certain functions from certain requests
    public void authorizeToken(Context ctx) {
        if(ctx.method().equals("OPTIONS")) {
            return;
        }
        String user = ctx.header("Username");                             // take in the username
        String token = ctx.header("Authorization");                              // HTTP header for authorization

        // this method passes the given token and username to the
        // token verification function to check that the token is valid
            boolean match = manager.authorize(token, user);
            if (!match) {
                logger.error("Token could not be verified for " + user);
                throw new UnauthorizedResponse();
            } else {
                logger.info("Token Verified for " + user);
            }
        }
    }