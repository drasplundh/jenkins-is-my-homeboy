package dev.asplundh.services;

import dev.asplundh.controllers.TokenManager;
import dev.asplundh.data.UserLoginDao;
import dev.asplundh.data.UserLoginDaoImpl;
import dev.asplundh.models.UserModel;
import io.javalin.http.UnauthorizedResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserService {

    private Logger logger = LoggerFactory.getLogger(UserService.class);
    private TokenManager manager;
    private UserLoginDao service = new UserLoginDaoImpl();

    public UserService() {
        //super();
    }

    public UserService(TokenManager manager) {
        this.manager = manager;
    }


    // this method takes in a UserModel object to be passed
    // to the UserDao class to grab the username and password from
    // the database.
    public boolean getCredentials(UserModel loginUser) {

        // this method checks to see if the credentials either:
        // match the ones we received from the client or:
        // if the credentials existed at all in the DB.
        boolean loginSuccess = false;
        UserModel userFromDatabase = service.getLoginCredentials(loginUser);
        if (loginUser.getUsername().equals(userFromDatabase.getUsername())) {           // Check if usernames match
            if (loginUser.getPassword().equals(userFromDatabase.getPassword())) {       // Check if passwords match
                loginSuccess = true;                                                    // if they do, return true
            } else {
                logger.error("Password does not match");                                // if passwords did not match
                throw new UnauthorizedResponse("Incorrect password for user: " + userFromDatabase.getUsername());                                       // log the error
            }

        } else {
            logger.error("User not found");                                             // if usernames did not match
            throw new UnauthorizedResponse("User not found");                                           // log the error

        }
        return loginSuccess;
    }

}
