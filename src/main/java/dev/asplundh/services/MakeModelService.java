package dev.asplundh.services;

import dev.asplundh.data.MakeModelHibDaoImpl;
import dev.asplundh.models.MakeModel;

import java.util.List;

public class MakeModelService {

    private MakeModelHibDaoImpl makeModelHibDao = new MakeModelHibDaoImpl();

    public List<MakeModel> getAllMakes() {
        return makeModelHibDao.getEvery();
    }

    public void deleteMakeByName(String name) {
         makeModelHibDao.deleteMake(name);
    }

    public void addMake(MakeModel make) { makeModelHibDao.addMakeName(make);}

}
