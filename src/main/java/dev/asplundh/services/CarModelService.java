package dev.asplundh.services;

import dev.asplundh.data.CarModelDao;
import dev.asplundh.data.CarModelHibDaoImpl;
import dev.asplundh.models.BigCarModel;
import dev.asplundh.models.CarModel;
import dev.asplundh.models.CarSpeed;

import java.util.HashMap;
import java.util.List;

public class CarModelService {

    private CarModelDao carModelDao = new CarModelHibDaoImpl();

    // No service logic needed, simply pass through to the carModelDao to get data
    public List<CarModel> getAll() { return carModelDao.getAllModels(); }

    public CarModel getCarByName(String name) {return carModelDao.getModelByName(name);}

    // No service logic needed, simply pass through to the carModelDao to add data
    public boolean add(CarModel model) {
        boolean success = carModelDao.addNewCarModel(model);
        return success;
    }

    // No service logic needed, simply pass through to the carModelDao to update data
    public void put(String id, String name) {
        carModelDao.putCarModel(id, name);
    }

    public void delete(String name) { carModelDao.deleteCarModel(name); }

    // This method handles the data retrieved from the database to determine which car is faster
    public String raceTopSpeed(String name1, String name2) {

        // CarSpeed is a DTO (data transfer object).
        // it is necessary because of the database structure.
        // The CarModel class does not have a field
        // for the cars' speeds and therefore cannot be used
        // in this case to compare them.
        List<CarSpeed> cars = carModelDao.raceByTopSpeed(name1, name2); // carModelDao that returns a CarSpeed object

        // Check to see if the cars have the same speed,
        // if not, check which is faster, declare the winner.
        if (cars.get(0).getSpeed() == cars.get(1).getSpeed()) {
            return ("It's a tie!");
        } else if (cars.get(0).getSpeed() > cars.get(1).getSpeed()) {
            return (cars.get(0).getName() + " with a top Speed of " + cars.get(0).getSpeed() + " is faster");
        } else {
            return (cars.get(1).getName() + " with a top Speed of " + cars.get(1).getSpeed() + " is faster");
        }
    }
        // This method is almost identical to the above method,
        // however it compares 0-60 speed (acceleration) instead
        // of top speed.
    public String raceAcceleration(String name1, String name2) {
        List<CarSpeed> cars = carModelDao.raceByAcceleration(name1, name2);
        if (cars.get(0).getAcceleration() == cars.get(1).getAcceleration()) {
            return ("It's a tie!");
        } else if (cars.get(0).getAcceleration() < cars.get(1).getAcceleration()) {
            return (cars.get(0).getName() + " with an Acceleration of 0-60 in " + cars.get(0).getAcceleration() + " seconds, is faster");
        } else {
            return (cars.get(1).getName() + " with an Acceleration of 0-60 in " + cars.get(1).getAcceleration() + " seconds, is faster");
        }
    }
}
