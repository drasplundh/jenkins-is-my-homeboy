package dev.asplundh.models;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@Table (name = "make")
@JsonIgnoreProperties("hibernateLazyInitializer")
public class MakeModel {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column (name = "make_id")
    private int makeId;
    private String name;
    private String region;

//    @OneToMany (fetch = FetchType.EAGER)
//    @JoinColumn (name="make_id", insertable = false, updatable = false)
    private CarModel carModel;

    public MakeModel() {
        //super();
    }

    public MakeModel(int makeId, String name, String region) {
        this.makeId = makeId;
        this.name = name;
        this.region = region;
    }

    public MakeModel(int makeId, String name, String region, CarModel carModel) {
        this.makeId = makeId;
        this.name = name;
        this.region = region;
        this.carModel = carModel;
    }

    public MakeModel(String name) {
        this.name = name;
    }

    public CarModel getCarModel() {
        return carModel;
    }

    public void setCarModel(CarModel carModel) {
        this.carModel = carModel;
    }

    public int getMakeId() {
        return makeId;
    }

    public void setMakeId(int makeId) {
        this.makeId = makeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
}
