package dev.asplundh.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table (name="model")
@JsonIgnoreProperties("hibernateLazyInitializer")
public class CarModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column (name="model_id")
    private int modelId;

    @Column (name="make_id")
    private int makeId;

    private String name;

    private String engine;

    @Column (name="top_speed")
    private double topSpeed;

    @Column (name="zero_to_sixty")
    private double zeroToSixty;

    @Column (name="mpg")
    private double mpg;

    private double price;

    @ManyToOne (fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name="make_id", insertable = false, updatable = false)
    private MakeModel makeModel;

    public CarModel() {
        super();
    }

    public CarModel(int modelId, int makeId, String name, String engine, double topSpeed, double zeroToSixty, double mpg, double price) {
        this.modelId = modelId;
        this.makeId = makeId;
        this.name = name;
        this.engine = engine;
        this.topSpeed = topSpeed;
        this.zeroToSixty = zeroToSixty;
        this.price = price;
        this.mpg = mpg;
    }

    public CarModel(int modelId, int makeId, String name, String engine,
                    MakeModel makeModel, double topSpeed, double zeroToSixty, double mpg, double price) {
        this.modelId = modelId;
        this.makeId = makeId;
        this.name = name;
        this.engine = engine;
        this.topSpeed = topSpeed;
        this.zeroToSixty = zeroToSixty;
        this.price = price;
        this.mpg = mpg;
        this.makeModel = makeModel;
    }

    public CarModel(String name) {
        this.name = name;
    }

    public MakeModel getMakeModel() {
        return makeModel;
    }

    public void setMakeModel(MakeModel makeModel) {
        this.makeModel = makeModel;
    }

    public double getMpg() {
        return mpg;
    }

    public void setMpg(double mpg) {
        this.mpg = mpg;
    }

    public int getModelId() {
        return modelId;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }

    public int getMakeId() {
        return makeId;
    }

    public void setMakeId(int makeId) {
        this.makeId = makeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public double getTopSpeed() {
        return topSpeed;
    }

    public void setTopSpeed(double topSpeed) {
        this.topSpeed = topSpeed;
    }

    public double getZeroToSixty() {
        return zeroToSixty;
    }

    public void setZeroToSixty(double zeroToSixty) {
        this.zeroToSixty = zeroToSixty;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}


