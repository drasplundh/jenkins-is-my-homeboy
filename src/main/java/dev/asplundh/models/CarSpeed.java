package dev.asplundh.models;

public class CarSpeed {
    private String name;
    private int speed;
    private double acceleration;

    public CarSpeed(String name, int speed) {
        this.name = name;
        this.speed = speed;
    }

    public CarSpeed(String name, double acceleration) {
        this.name = name;
        this.acceleration = acceleration;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public double getAcceleration() {
        return acceleration;
    }

    public void setAcceleration(double acceleration) {
        this.acceleration = acceleration;
    }
}
