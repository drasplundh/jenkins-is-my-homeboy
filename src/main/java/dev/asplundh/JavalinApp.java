package dev.asplundh;

import dev.asplundh.controllers.AuthController;
import dev.asplundh.controllers.CarMakeController;
import dev.asplundh.controllers.CarModelController;
import dev.asplundh.util.SecurityUtil;
import io.javalin.Javalin;
import io.javalin.core.JavalinConfig;

import static io.javalin.apibuilder.ApiBuilder.*;



public class JavalinApp {

    SecurityUtil securityUtil = new SecurityUtil();
    AuthController authController = new AuthController();
    CarModelController carModelController = new CarModelController();
    CarMakeController carMakeController = new CarMakeController();

    Javalin app = Javalin.create(JavalinConfig::enableCorsForAllOrigins).routes(()->{
            path("/carmodels", () -> {
                before("/", authController::authorizeToken);
                get(carModelController::handleGetCarModelRequest);
                post(carModelController::handlePostCarModelRequest);
                put(carModelController::handlePutCarModelRequestById);
                delete(carModelController::handleDeleteCarModelRequestByName);
                path(":name", () -> {
                    before("/", authController::authorizeToken);
                    post(carModelController::handleGetCarModelByNameRequest);
                });
            });
            path("/login", () -> {
                post(authController::login);
                after("/", securityUtil::attachResponseHeaders);
            });

            path("/make", () -> {
                get(carMakeController::handleGetCarMakeRequest);
                post(carMakeController::handleAddMakeRequest);
                delete(carMakeController::handleDeleteMakeByNameRequest);
            });
        });



    public void start(int port) {
        this.app.start(port);
    }

    public void stop() {
        this.app.stop();
    }

}
