//package dev.asplundh.services;
//
//import dev.asplundh.data.UserLoginDao;
//import dev.asplundh.models.UserModel;
//import dev.asplundh.services.UserService;
//import io.javalin.http.UnauthorizedResponse;
//import org.junit.jupiter.api.Test;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.mockito.Mockito.when;
//
//public class UserServiceTest {
//
//    @InjectMocks
//    UserService testUser;
//
//    @Mock
//    UserLoginDao service;
//
//    @Test
//    public void getCredentialsSuccessTest() {
//        UserModel loginUser = new UserModel("duncan", "password");
//        UserModel userFromDatabase = new UserModel("duncan", "password");
//        when(service.getLoginCredentials(loginUser)).thenReturn(userFromDatabase);
//        assertEquals(loginUser.getUsername(), testUser.getCredentials(loginUser));
//        assertEquals(loginUser.getPassword(), testUser.getCredentials(loginUser));
//    }
//}
