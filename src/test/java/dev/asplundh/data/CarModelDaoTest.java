//package dev.asplundh.data;
//
//import dev.asplundh.models.CarModel;
//import dev.asplundh.util.ConnectionUtil;
//import org.junit.jupiter.api.Test;
//import org.mockito.Mock;
//
//import java.sql.Connection;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.sql.Statement;
//import java.util.ArrayList;
//import java.util.List;
//
//public class CarModelDaoTest {
//
//    @Mock
//    Connection connection;
//
//    @Mock
//    Statement statement;
//
//    @Mock
//    ResultSet resultSet;
//
//
//    @Test
//    public void getAllModelsSuccessTest() {
//            List<CarModel> cars = new ArrayList<>();
//            try (Connection connection = ConnectionUtil.getConnection()) {
//                Statement statement = connection.createStatement();
//                ResultSet resultSet = statement.executeQuery("select * from model");
//
//                while (resultSet.next()) {
//                    int model_id = resultSet.getInt("model_id");
//                    int make_id = resultSet.getInt("make_id");
//                    String name = resultSet.getString("name");
//                    CarModel model = new CarModel(model_id, make_id, name);
//                    cars.add(model);
//                }
//
//            } catch (SQLException e) {
//                logger.error(e.getClass() + " " + e.getMessage());
//            }
//            return cars;
//        }
//    }
