//package dev.asplundh.controllers;
//
//import dev.asplundh.controllers.CarModelController;
//
//import dev.asplundh.models.CarModel;
//import dev.asplundh.services.CarModelService;
//import io.javalin.http.Context;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import static org.mockito.Mockito.*;
//
//public class CarModelControllerTest {
//
//    @InjectMocks
//    private CarModelController carModelController;
//
//    @Mock
//    private CarModelService service;
//
//    @BeforeEach
//    public void initMocks(){
//        MockitoAnnotations.initMocks(this);
//    }
//
//    @Test
//    public void testGetCarModelRequest(){
//        Context context = mock(Context.class);
//
//        List<CarModel> cars = new ArrayList<>();
//        cars.add(new CarModel(1,1, "Sagaris"));
//        cars.add(new CarModel(2,4, "Diablo"));
//        when(service.getAll()).thenReturn(cars);
//        carModelController.handleGetCarModelRequest(context);
//        verify(context).json(cars);
//    }
//
//    @Test
//    public void testHandlePostCarMethodRequest() {
//        CarModel car = new CarModel(1, 4, "Enzo");
//        Context context = mock(Context.class);
//        carModelController.handlePostCarModelRequest((context));
//        verify(context).status(201);
//    }
//
//    @Test
//    public void testHandleRaceByTopSpeed() {
//        Context context = mock(Context.class);
//        String name1 = "Bronco";
//        String name2 = "Countach";
//        String winner = "The Countach is faster";
//        when(service.raceTopSpeed(name1, name2)).thenReturn(winner);
//        verify(context).json(winner);
//    }
//
//}
